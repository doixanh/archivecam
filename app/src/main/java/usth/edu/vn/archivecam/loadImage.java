package usth.edu.vn.archivecam;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * Created by Khoa on 02-Jun-16.
 */
public class loadImage extends AsyncTask<String, Void, Bitmap> {
    private ImageView mIV;
    private Context mContext;

    public loadImage(Context context, ImageView iv){
        mContext = context;
        mIV = iv;
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        InputStream is = null;
        try {
            is = mContext.openFileInput(params[0]);
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }
        Bitmap bm = null;
        if(is != null) {
            bm = BitmapFactory.decodeStream(is);
            bm = Bitmap.createScaledBitmap(bm, bm.getWidth(), bm.getHeight(), true);
        }
        return bm;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        mIV.setImageBitmap(bitmap);
    }
}
