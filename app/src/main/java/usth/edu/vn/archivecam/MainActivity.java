package usth.edu.vn.archivecam;


import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import usth.edu.vn.archivecam.adapter.ViewPagerAdapter;

public class MainActivity extends AppCompatActivity {
    protected final static String TAG_OPENCV = "OpenCV";
    protected final static String TAG_DIR = "Directory";
    protected final static String tempFile = "picture";
    protected ArrayList<Bitmap> bitmapArray = new ArrayList<Bitmap>();

    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
    private Uri imageUri;

    private int NUM_PAGE = 0;

    private PagerAdapter adapter = null;
    private ViewPager pager = null;

    static {
        if(!OpenCVLoader.initDebug()){
            Log.e(TAG_OPENCV, "Fail to init OpenCV");
        }
    }

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch(status){
                case BaseLoaderCallback.SUCCESS:
                {
                    Log.i(TAG_OPENCV, "OpenCV loaded");
                    System.out.println("Loaded");
                }break;
                default:
                {
                    super.onManagerConnected(status);
                }break;
            }
        }
    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        takeImageIntent();

    }

    private File createTemporaryFile() throws Exception{
        File tempDir = Environment.getExternalStorageDirectory();
        tempDir = new File(tempDir.getAbsolutePath()+"/.temp/");
        if (!tempDir.exists()){
            tempDir.mkdir();
        }

        return File.createTempFile(tempFile, ".jpg", tempDir);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE){
            if(resultCode == RESULT_OK){
                this.getContentResolver().notifyChange(imageUri, null);
                ContentResolver cr = this.getContentResolver();
                Bitmap bm = null;
                try{
                    bm = android.provider.MediaStore.Images.Media.getBitmap(cr, imageUri);
                }catch (Exception e){
                    Toast.makeText(this, "Failed to load", Toast.LENGTH_SHORT).show();
                    Log.d(TAG_DIR, "Failed to load", e);
                }

                new processImage(bm).execute();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void takeImageIntent(){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photo = null;

        try{
            photo = this.createTemporaryFile();
            photo.delete();
        }catch (Exception e){
            Log.v(TAG_DIR, "Can't create File");
            Toast.makeText(this, "Please check SD card", Toast.LENGTH_SHORT).show();
        }

        imageUri = Uri.fromFile(photo);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
    }

    private class processImage extends AsyncTask<Void, Integer, Integer>{
        private Bitmap mBitmap;
        public processImage(Bitmap bitmap){
            mBitmap = bitmap;
        }

        @Override
        protected Integer doInBackground(Void... params) {
            Context context = getApplicationContext();
            documentFinder DF = new documentFinder();
            Bitmap preResult = DF.documentDetect(mBitmap);

            saveBitmapToFile saveBitmapToFile = new saveBitmapToFile();

            Mat inputFrame = DF.getInputFrame();
            List<MatOfPoint> contours = DF.getContours();
            int largest_contour_index = DF.getLargest_contour_index();
            Point[] pointRect = DF.getPointRect();
            int numPoint = DF.getNumPoint();

            Matrix mat = new Matrix();
            mat.postRotate(90);
            mBitmap = Bitmap.createBitmap(mBitmap, 0 ,0 , mBitmap.getWidth(), mBitmap.getHeight(), mat, true);
            mBitmap = Bitmap.createScaledBitmap(mBitmap, mBitmap.getWidth()/2, mBitmap.getHeight()/2, true);
            preResult = Bitmap.createScaledBitmap(preResult, preResult.getWidth()/2, preResult.getHeight()/2, true);
            saveBitmapToFile.saveToInternal(context, mBitmap, "photo.png");
            saveBitmapToFile.saveToInternal(context, preResult, "preResult.png");

            if(!DF.checkRect(numPoint)){
                NUM_PAGE = 2;
            }
            else {
                NUM_PAGE =3 ;
                Bitmap result = DF.documentExtractor(inputFrame, contours, largest_contour_index, pointRect, numPoint);
                saveBitmapToFile.saveToInternal(context, result, "result.png");
            }
            return NUM_PAGE;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            System.out.println(integer);
            super.onPostExecute(integer);
            adapter = new ViewPagerAdapter(getSupportFragmentManager());

            pager = (ViewPager)findViewById(R.id.viewpager);
            pager.setOffscreenPageLimit(integer);
            pager.setAdapter(adapter);
        }
    }

}


