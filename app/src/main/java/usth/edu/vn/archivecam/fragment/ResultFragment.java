package usth.edu.vn.archivecam.fragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import usth.edu.vn.archivecam.R;
import usth.edu.vn.archivecam.loadImage;

/**
 * Created by Khoa on 25-May-16.
 */
public class ResultFragment extends Fragment {
    public ResultFragment(){}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_result, null);

        final ImageView iv = (ImageView)v.findViewById(R.id.result_image);
        iv.setDrawingCacheEnabled(true);
        Button bt = (Button)v.findViewById(R.id.button);

        new loadImage(getActivity(), iv).execute("result.png");



        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OutputStream fos = null;
//                Uri outputFileUri;

                try{
                    File root = new File(Environment.getExternalStorageDirectory() + File.separator + "ARCHIVECam" + File.separator);
                    root.mkdirs();
                    File sdImageMainDir = new File(root, getCurrentTime());
//                    outputFileUri = Uri.fromFile(sdImageMainDir);
                    fos = new FileOutputStream(sdImageMainDir);
                }catch (Exception e){
                    Toast.makeText(getActivity(), "Error occurred. Please try again later.", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

                try{
                    Bitmap bm = iv.getDrawingCache();
                    bm.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                    fos.close();
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });

        return v;
    }

    public String getCurrentTime(){
        String currentTime;

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd_HHmmss");
        currentTime = df.format(c.getTime()) + ".jpg";

        return currentTime;
    }
}
