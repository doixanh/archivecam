package usth.edu.vn.archivecam.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


import usth.edu.vn.archivecam.fragment.PhotoFragment;
import usth.edu.vn.archivecam.fragment.PreResultFragment;
import usth.edu.vn.archivecam.fragment.ResultFragment;

/**
 * Created by Khoa on 25-May-16.
 */
public class ViewPagerAdapter extends FragmentPagerAdapter {
    final int PAGE_COUNT = 3;
    public ViewPagerAdapter(FragmentManager fm){
        super(fm);
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }



    @Override
    public Fragment getItem(int position) {

        switch (position){
            case 0: return new PhotoFragment();
            case 1: return new PreResultFragment();
            case 2: return new ResultFragment();
        }

        return null;
    }

}
